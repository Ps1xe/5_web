import { createElement } from './index.js';

//Sample element
// const sample = {
//   tag: 'div',
//   attributes: { style },
//   child: [{ tag: 'span' }],
// };

/**
 *
 * @param {HTMLElement} root -- корневой элемент, его потомком является отрендеренный element
 * @param {object} element -- объект для рендеринга.
 *   Должен содержать ключ tag -- тег, который нужно отрендерить;
 *   Может содержать ключ attributes -- объект, с атрибутами элемента;
 *   Может содержать ключ child -- массив точно таких же элементов, как и element.
 *   Пример element описан выше.
 */
export function initializeApp(root, element) {
  root.appendChild(createElement(element));
}
