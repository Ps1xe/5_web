/**
 * Функция должна добавлять html-элементу root потомка childOrChildren
 * @param {HTMLElement} root -- родительский элемент
 * @param {string[] | string | HTMLElement | HTMLElement[]} childOrChildren
 */
 function parseChild(root, childOrChildren){
  var text;
  if (Array.isArray(childOrChildren)){
    if (typeof(childOrChildren[0]) === typeof("String")){
      text = childOrChildren.join('');
      root.innerHTML = text;
    } 
      else{
        childOrChildren.forEach(el=>{
          root.appendChild(createElement(el))
        });
    }
  } 
  else{
    if (typeof(childOrChildren) === typeof("String")){
      root.innerHTML = childOrChildren;
    } 
    else {
      root.appendChild(createElement(childOrChildren))
    }
  }
  return root
}


/**
 *
 * Функция для валидации переданных параметров для createElement
 * @param {object} params -- параметры createElement
 */
function validateParams(params) {
  console.log(params);
  if ((!params.tag && !(params instanceof HTMLElement)) || !params) {
    throw "Wrong params";
  }
  return true
}


/**
 *
 * @param {object} params -- Параметры для создания HTMLElement:
 *   tag -- html-тэг элемента;
 *   attributes -- html-атрибуты элемента;
 *   child -- потомки/потомок
 * @returns HTMLElement
 */
export function createElement(params) {
  validateParams(params)
  if (params instanceof HTMLElement){
    var newElement = document.createElement(params.tagName);
  }
  else
    var newElement = document.createElement(params.tag);
  for (var attribute in params.attributes) {
    newElement.setAttribute(attribute, params.attributes[attribute]);
  }

  if (params.child) {
    newElement = parseChild(newElement, params.child);
  }
  return newElement;
}
