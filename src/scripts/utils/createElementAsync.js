import { createElement } from './createElement.js';

/**
 *
 * @param {function} fetchFunction - функция, которая делает запрос на сервер и возвращает полученный ответ
 * @param {*} mapFunction - функция, которая получает трансформирует ответ сервера в объект для рендеринга
 * @returns HTMLElement
 */
export async function createElementAsync(fetchFunction, mapFunction) {
  return createElement(mapFunction(fetchFunction()));
}
