//API -- https://jsonplaceholder.typicode.com/


//Нужно найти в документации jsonplaceholder где лежат тудушки
// const response = await fetch(/**/);

const response = await fetch('https://jsonplaceholder.typicode.com/todos');

//Нужно привести сырой ответ сервера в формат JSON
//const todos = await response.???();

const todos = await response.json();
console.log(todos);
/**
 * Функция ответственная за рендеринг ответа с сервера
 */
const renderedTodos = todos.map((todo) => {
  /**
   ! Обратите внимание, что если JSONPlaceholder отдает завершенную
   тудушку, то мы должны помечать это через атрибут checked для элемента input
  */
  var todoElement = {tag:"div"}
  var textTodo = {tag:"span",child:todo.title}
  var checkboxTodoElement = {tag: "input", attributes: {type: "checkbox"}}
  if (todo.completed) {
    checkboxTodoElement.attributes.checked=true;
  }
  todoElement.child=[textTodo,checkboxTodoElement]
  return todoElement;
});

//Элемент, к которому нужно добавить отрендеренные тудушки
//К этому элементу в дальнейшем будет применена функция createElement
const app = {
  tag: 'div',
  child: [],
};

app.child = renderedTodos;

export default app;
