import { initializeApp } from './scripts/index.js';
import App from './layout/index.html.js';

//Получаем html-элемент, потомком которого будут отрендеренные элементы

const root = document.getElementById('root');

//Здесь мы инициализируем рендеринг нашего списка тудушников из layout/index.html.jsx
initializeApp(root, App);
